import { Component, OnInit } from '@angular/core';
import Type from '../../models/Type';
import { TypeService } from '../../services/type.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-type-get',
  templateUrl: './type-get.component.html',
  styleUrls: ['./type-get.component.css']
})

export class TypeGetComponent implements OnInit {

  type: Type[];

  constructor( private bs: TypeService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getType()
      .subscribe((data: Type[]) => {
        this.type = data;
    });
  }

  deleteType(id) {
    this.bs.deleteType(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getType()
      .subscribe((data: Type[]) => {
        this.type = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

