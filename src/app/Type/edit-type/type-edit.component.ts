import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TypeService } from '../../services/type.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-type-edit',
  templateUrl: './type-edit.component.html',
  styleUrls: ['./type-edit.component.css']
})
export class TypeEditComponent implements OnInit {
     angForm: FormGroup;
  type: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: TypeService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        name: ['', Validators.required ],
description: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editType(params['id']).subscribe(res => {
        this.type = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateType(name, description ) {
   this.route.params.subscribe(params => {
      this.bs.updateType(name, description  , params['id']);
      this.showInfo();
      this.router.navigate(['type']);
   });
}
}
