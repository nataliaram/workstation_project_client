import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { WorkstationAddComponent } from './Workstation/add-workstation/workstation-add.component';
import { WorkstationEditComponent } from './Workstation/edit-workstation/workstation-edit.component';
import { WorkstationGetComponent } from './Workstation/get-workstation/workstation-get.component';
import { TypeAddComponent } from './Type/add-type/type-add.component';
import { TypeEditComponent } from './Type/edit-type/type-edit.component';
import { TypeGetComponent } from './Type/get-type/type-get.component';

const routes: Routes = [
  {
  path: '',
  component: HomeComponent
},
  {
  path: 'workstation/create',
  component: WorkstationAddComponent
},
{
  path: 'workstation/edit/:id',
  component: WorkstationEditComponent
},
{
  path: 'workstation',
  component: WorkstationGetComponent
},
{
  path: 'type/create',
  component: TypeAddComponent
},
{
  path: 'type/edit/:id',
  component: TypeEditComponent
},
{
  path: 'type',
  component: TypeGetComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
