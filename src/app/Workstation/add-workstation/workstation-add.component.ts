import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Type from '../../models/Type';
import { WorkstationService } from '../../services/workstation.service';
import { TypeService } from '../../services/type.service';
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-workstation-add',
  templateUrl: './workstation-add.component.html',
  styleUrls: ['./workstation-add.component.css']
})
export class WorkstationAddComponent implements OnInit {
   Type: Type[];   angForm: FormGroup;
  constructor(
private TypeSer: TypeService,
   private fb: FormBuilder,
   private Workstation_ser: WorkstationService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       name: ['', Validators.required ],
laboratory: ['', Validators.required ],
implement: ['', Validators.required ],
type: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addWorkstation(name, laboratory, implement, type ) {
    this.Workstation_ser.addWorkstation(name, laboratory, implement, type );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
    this.TypeSer
.getType()
.subscribe((data: Type[]) => {
this.Type = data;
});
  }

}
