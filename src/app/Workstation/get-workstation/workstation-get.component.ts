import { Component, OnInit } from '@angular/core';
import Workstation from '../../models/Workstation';
import { WorkstationService } from '../../services/workstation.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-workstation-get',
  templateUrl: './workstation-get.component.html',
  styleUrls: ['./workstation-get.component.css']
})

export class WorkstationGetComponent implements OnInit {

  workstation: Workstation[];

  constructor( private bs: WorkstationService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getWorkstation()
      .subscribe((data: Workstation[]) => {
        this.workstation = data;
    });
  }

  deleteWorkstation(id) {
    this.bs.deleteWorkstation(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getWorkstation()
      .subscribe((data: Workstation[]) => {
        this.workstation = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

