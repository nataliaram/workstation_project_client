import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Type from '../../models/Type';
import { WorkstationService } from '../../services/workstation.service';
import { TypeService } from '../../services/type.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-workstation-edit',
  templateUrl: './workstation-edit.component.html',
  styleUrls: ['./workstation-edit.component.css']
})
export class WorkstationEditComponent implements OnInit {
  Type: Type[];   angForm: FormGroup;
  workstation: any = {};

  constructor(private route: ActivatedRoute, private TypeSer: TypeService,
    private router: Router,
    public toastr: ToastrManager,
    private bs: WorkstationService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        name: ['', Validators.required ],
laboratory: ['', Validators.required ],
implement: ['', Validators.required ],
type: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editWorkstation(params['id']).subscribe(res => {
        this.workstation = res;
      });
    });
     this.TypeSer
.getType()
.subscribe((data: Type[]) => {
this.Type = data;
});
   }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateWorkstation(name, laboratory, implement, type ) {
   this.route.params.subscribe(params => {
      this.bs.updateWorkstation(name, laboratory, implement, type  , params['id']);
      this.showInfo();
      this.router.navigate(['workstation']);
   });
}
}
