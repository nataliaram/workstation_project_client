import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { WorkstationAddComponent } from './Workstation/add-workstation/workstation-add.component';
import { WorkstationEditComponent } from './Workstation/edit-workstation/workstation-edit.component';
import { WorkstationGetComponent } from './Workstation/get-workstation/workstation-get.component';
import { TypeAddComponent } from './Type/add-type/type-add.component';
import { TypeEditComponent } from './Type/edit-type/type-edit.component';
import { TypeGetComponent } from './Type/get-type/type-get.component';

import { ToastrModule } from 'ng6-toastr-notifications';
import { SidebarjsModule } from 'ng-sidebarjs';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { WorkstationService } from './services/workstation.service';
import { TypeService } from './services/type.service';


@NgModule({
  declarations: [
     HomeComponent,
    AppComponent,
WorkstationAddComponent,
WorkstationGetComponent,
WorkstationEditComponent
,
TypeAddComponent,
TypeGetComponent,
TypeEditComponent

  ],
  imports: [
    NgbModule,
    SidebarjsModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    ToastrModule.forRoot()
  ],
  providers: [
    WorkstationService,
TypeService

    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
