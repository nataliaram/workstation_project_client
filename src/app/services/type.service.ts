import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TypeService {

  uri = 'http://localhost:3000/type';

  constructor(private http: HttpClient) { }
  addType(name , description ) {
    const obj = {
      name: name,
description: description

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getType() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editType(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateType(name , description , id) {

    const obj = {
      name: name,
description: description

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteType(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
