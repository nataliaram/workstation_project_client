import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WorkstationService {

  uri = 'http://localhost:3000/workstation';

  constructor(private http: HttpClient) { }
  addWorkstation(name , laboratory , implement , type ) {
    const obj = {
      name: name,
laboratory: laboratory,
implement: implement,
type: type

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getWorkstation() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editWorkstation(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateWorkstation(name , laboratory , implement , type , id) {

    const obj = {
      name: name,
laboratory: laboratory,
implement: implement,
type: type

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteWorkstation(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
